# Teste Front-end Gabriel Marques

O projeto consiste na entrega de um front-end de uma landing page.

### Observações

O projeto não está 100% completo, não **contemplando a parte de responsividade** (mobile). **Apenas a versão desktop** (1440px +) pode ser testada e avaliada.

100% do código escrito nesse projeto é de minha autoria (exceto as bibliotecas utilizadas).

Optei por não utilizar nenhum framework de layout para poder demonstrar melhor minhas habilidades em escrever CSS.

---

### Tencnoogias (linguagens) utilizadas

- HTML

- CSS

- SCSS

- GULP

- NODEJS (apenas NPM)

- JavaScript Vanilla (sem frameworks)

---

### Dev Dependecies

- NODEJS

- gulp: ^4.0.2

- gulp-sass: ^5.1.0

- node-sass: ^7.0.3

- sass:  ^1.55.0

Essas dependências são necessárias apenas para criar o ambiente de desenvolvimento local, elas não impdem de baixar e rodar o projeto em sua máquina.

---

### Dependencias (Libs)

##### sweetalert2

Bibliotéca CSS leve utilizada para criar efeitos de alert mais atraentes de forma rápida e fácil.

##### AOS

Bibliotéca CSS leve utilizada para animar elementos na página, efeito de aparecer e entradas mais suaves

##### Leaflet Maps API

API utilizada para criar o mapa do rodapé.
