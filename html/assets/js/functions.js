document.addEventListener("DOMContentLoaded", function(){
    //cria o mapa 
    var map = L.map('map').setView([51.505, -0.09], 13);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    //cria marcador no mapa
    var marker = L.marker([51.5, -0.09]).addTo(map);
    
    // inicia AOS
    AOS.init();

    //valida email
    function validateEmail(inputField) {
        let fieldEmail = document.querySelector(inputField).value;
        if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldEmail)){
            Swal.fire('OOOps!&#x270B;','Digite seu email e certifique-se de que está no formato correto.','error');
            return false;
        } else {
            return true;
        }
    }

    //valida campo vazio
    function validaVazio(inputField){
        let fieldVazio = document.querySelector(inputField).value;
        if(fieldVazio == "" || fieldVazio == "Your Message") {
            Swal.fire('OOOps!&#x270B;','Atenção, nenhum campo ficar vazio.','error');
            return false;
        } else {
            return true;
        }
    }

    //cadastrar email no header
    document.getElementById("top-header__form__submit").addEventListener("click", function(event){
        event.preventDefault();
        if(validateEmail("#top-header__form_email")){
            Swal.fire('Aí sim!&#x1F609;','Email cadastrado com sucesso.','success');
            document.querySelector(".top-header__form").reset();
        }
    })

    //cadastrar email no body
    document.getElementById("subscribe_form_submit").addEventListener("click", function(event){
        event.preventDefault();
        if(validateEmail("#subscribe_form_field")){
            Swal.fire('Aí sim!&#x1F609;','Email cadastrado com sucesso.','success');
            document.querySelector(".subscribe__form").reset();
        }
    })

    //valida form de contato
    document.getElementById("contact-form__submit").addEventListener("click", function(event){
        event.preventDefault();
        if(validaVazio("#contact-form__name") && validateEmail("#contact-form__email") && validaVazio("#contact-form__message")){
            Swal.fire('Aí sim!&#x1F609;','Mensagem enviada com sucesso.','success');
            document.querySelector(".contact-form").reset();
        }
    })
});
