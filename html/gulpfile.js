const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
sass.compiler = require('node-sass');

gulp.task('default', watch);
gulp.task('sass', compileScss);


function compileScss(){
    return gulp
        .src('scss/*.scss')
        .pipe(sass()
        .on('error', sass.logError))
        .pipe(gulp.dest('assets/css'))
}

function watch() {
    gulp.watch('scss/*.scss', compileScss);
}